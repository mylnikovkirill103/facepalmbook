import { Chat } from 'stream-chat-react'
import { StreamChat } from 'stream-chat'
import { API_KEY } from './global';
import Cookies from 'universal-cookie'

import ChannelListContainer from './components/ChannelListContainer';

import './App.css';

const client = StreamChat.getInstance(API_KEY);

function App() {
  return (
    <div className="app__wrapper">
      <Chat client={client} theme="team light">
        <ChannelListContainer>

        </ChannelListContainer>
      </Chat>
    </div>
  );
}

export default App;
