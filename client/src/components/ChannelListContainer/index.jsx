import React from 'react';
import Cookies from 'universal-cookie';
import { ChannelList, useChatContext } from 'stream-chat-react';
import SideBar from '../SideBar';
import CompanyHeader from '../CompanyHeader';
import ChannelSearch from '../ChannelSearch';
import TeamChannelList from '../TeamChannelList';
import TeamChannelPreview from '../TeamChannelPreview';

export default function ChannelListContainer() {
    return (
        <>
            <SideBar />
            <div className="channel-list__list__wrapper">
                <CompanyHeader />
                <ChannelSearch />
                <ChannelList
                    filters={{}}
                    channelRenderFilterFn={() => { }}
                    List={(listProps) =>
                        <TeamChannelList {...listProps} type="team" />}
                    Preview={(previewProps) => <TeamChannelPreview {...previewProps} />} />
            </div>
        </>
    )
}
