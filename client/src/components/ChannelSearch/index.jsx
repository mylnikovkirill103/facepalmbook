import React, { useState } from 'react'
import { useChatContext } from 'stream-chat-react';
import { SearchIcon } from '../../assets/SearchIcon'

export default function ChannelSearch() {
    const [value, setValue] = useState('');
    const [loading, setLoading] = useState(false);

    const getChannels = async (text) => {
        try {

        } catch (error) {
            setValue('')
        }
    }

    const handlerChange = (e) => {
        setValue(e.target.value);
        setLoading(true);
        getChannels(e.target.value);
    };

    return (
        <div className="channel-search__container">
            <div className="channel-search__input__wrapper">
                <div className="channel-search__input-icon">
                    <SearchIcon />
                </div>
                <input
                    className="channel-search__input__text"
                    placeholder="Search"
                    type="text"
                    value={value}
                    onChange={handlerChange} />
            </div>
        </div>
    )
}
