import React from 'react'

export default function CompanyHeader() {
    return (
        <div className="channel-list__header">
            <p className="channel-list__header__text">
                Medical Pager
            </p>
        </div>
    )
}
