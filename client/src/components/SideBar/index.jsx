import React from 'react'
import HospitalIcon from '../../assets/hospital.png'
import LogoutIcon from '../../assets/logout.png'

export default function SideBar() {
    return (
        <div className="channel-list__sidebar">
            <div className="channel-list__sidebar__icon1">
                <div className="icon1__inner">
                    <img src={HospitalIcon} alt="Hospital" width="30" />
                </div>
            </div>
            <div className="channel-list__sidebar__icon2">
                <div className="icon1__inner">
                    <img src={LogoutIcon} alt="logout" width="30" />
                </div>
            </div>
        </div>
    )
}
