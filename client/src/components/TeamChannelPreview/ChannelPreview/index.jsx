import React from 'react'

export default function ChannelPreview({ channel }) {
    return (
        <p className="channel-preview__item">
            # {channel?.data?.name || channel?.data?.id}
        </p>
    )
}
