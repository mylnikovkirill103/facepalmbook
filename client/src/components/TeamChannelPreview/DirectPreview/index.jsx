import React from 'react'
import { Avatar } from 'stream-chat-react';

export default function DirectPreview({ channel, client }) {

    const members = Object.values(channel.state.members).filter((user) => user.id === client.userID);

    return (
        <div className="channel-preview__itemm single">
            <Avatar image={members[0]?.user?.image}
                name={members[0]?.user?.fullName}
                size={24}
            />
        </div>
    )
}
