import React from 'react'
import { Avatar, useChatContext } from 'stream-chat-react';
import ChannelPreview from './ChannelPreview';
import DirectPreview from './DirectPreview';

export default function TeamChannelPreview({ channel, type }) {
    const { channel: activeChannel, client } = useChatContext();

    return (
        <div className={channel?.id === activeChannel?.id ? 'channel-preview__selected' : "channel-preview__wrapper"}>
            <ChannelPreview channel={channel} />
            <DirectPreview channel={channel} client={client}/>
        </div>
    )
}
